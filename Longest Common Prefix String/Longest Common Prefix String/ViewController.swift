//
//  ViewController.swift
//  Longest Common Prefix String
//
//  Created by Daniel Filho on 29/06/18.
//  Copyright © 2018 Daniel Griso Filho. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    //MARK: Local Variables
    fileprivate var strings: [String] = []
    fileprivate var maxString = ""

    //MARK: Local Outlets
    @IBOutlet weak var lblTitleLoading: UILabel!
    @IBOutlet weak var lblBottomLoading: UILabel!
    @IBOutlet weak var lblLongestCommonPrefix: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    //MARK: - App Life Cicle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.configurateViewComponents(isSearchPrefix: true)
        DispatchQueue.main.async {
            self.readFile()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Intern Functions
    
    //MARK: Configurate View Components
    fileprivate func configurateViewComponents(isSearchPrefix: Bool) {
        if isSearchPrefix {
            self.activityIndicator.startAnimating()
            self.lblLongestCommonPrefix.isHidden = true
            self.lblTitleLoading.isHidden = false
            self.lblBottomLoading.isHidden = false
        }else {
            self.activityIndicator.stopAnimating()
            self.lblLongestCommonPrefix.isHidden = false
            self.lblTitleLoading.isHidden = true
            self.lblBottomLoading.isHidden = true
        }
    }
    
    //MARK: Read Internal File
    fileprivate func readFile() {
        let path = Bundle.main.path(forResource: "strings", ofType: "")
        
        do {
            let text = try String(contentsOfFile: path!, encoding: .utf8)
            let myString = text.components(separatedBy: .newlines)
            
            self.strings = myString
            
            let result = self.longestCommonPrefix()
            self.configurateViewComponents(isSearchPrefix: false)
            self.lblLongestCommonPrefix.text = "The Longest Common Prefix String is: \(result)"
        } catch {
            self.configurateViewComponents(isSearchPrefix: false)
            self.lblLongestCommonPrefix.text = "\(error)"
        }
    }
    
    //MARK: Find The Longest Prefix
    fileprivate func longestCommonPrefix() -> String {
        var prefix = ""
        
        for str in strings {
            for char in str {
                let newPrefix = prefix + String(char)
                let allContainPrefix = strings.reduce(0) {
                    $1.hasPrefix(newPrefix) ? $0 + 1 : $0
                } > 1
                
                if allContainPrefix {
                    prefix = newPrefix
                }else {
                    strings.remove(at: 0)
                    
                    if prefix.count > maxString.count {
                        maxString = prefix
                    }
                    
                    prefix = ""
                    break
                }
            }
        }
        return maxString
    }
}
