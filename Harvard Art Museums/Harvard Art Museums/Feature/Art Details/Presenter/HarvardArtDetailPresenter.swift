//
//  HarvardArtDetailPresenter.swift
//  Harvard Art Museums
//
//  Created Daniel Griso Filho on 7/1/18.
//  Copyright © 2018 Daniel Griso Filho. All rights reserved.

import UIKit

class HarvardArtDetailPresenter {

    //MARK: Local Variable
    var view: HarvardArtDetailViewControllerProtocol?
    var interactor: HarvardArtDetailInteractorProtocol?
    var router: HarvardArtDetailRouterProtocol?

    //MARK: Initializer
    init(view: HarvardArtDetailViewControllerProtocol, interactor: HarvardArtDetailInteractorProtocol, router: HarvardArtDetailRouterProtocol) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    //MARK: - Functions
}

//MARK: - Extension to implement HarvardArtDetailPresenterProtocol
extension HarvardArtDetailPresenter: HarvardArtDetailPresenterProtocol {
    
}
