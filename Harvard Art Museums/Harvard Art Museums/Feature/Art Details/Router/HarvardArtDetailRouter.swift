//
//  HarvardArtDetailRouter.swift
//  Harvard Art Museums
//
//  Created Daniel Griso Filho on 7/1/18.
//  Copyright © 2018 Daniel Griso Filho. All rights reserved.


import UIKit

class HarvardArtDetailRouter: BaseRouter {
    
    //MARK: - Functions
    static func registerWithNavigation() -> UINavigationController {
        return UINavigationController(rootViewController: createModule())
    }
    
    static func createModule() -> HarvardArtDetailViewController {
        // Change to get view from storyboard if not using progammatic UI
        let viewController: HarvardArtDetailViewController = HarvardArtDetailViewController()
        let router: HarvardArtDetailRouter = HarvardArtDetailRouter(viewController: viewController)
        let interactor: HarvardArtDetailInteractor = HarvardArtDetailInteractor()
        let presenter: HarvardArtDetailPresenter = HarvardArtDetailPresenter(view: viewController, interactor: interactor, router: router)
        
        viewController.presenter = presenter
        interactor.presenter = presenter
        router.viewController = viewController
        
        return viewController
    }
}

//MARK: - Extension to implement HarvardArtDetailRouterProtocol
extension HarvardArtDetailRouter: HarvardArtDetailRouterProtocol {
    
}
