//
//  HarvardArtDetailViewController.swift
//  Harvard Art Museums
//
//  Created Daniel Griso Filho on 7/1/18.
//  Copyright © 2018 Daniel Griso Filho. All rights reserved.

import UIKit
import SDWebImage

class HarvardArtDetailViewController: UIViewController {

    //MARK: Local Variable
	var presenter: HarvardArtDetailPresenterProtocol?
    var detailArt = [Records]()
    
    //MARK: Local Outlets
    @IBOutlet weak var imgArt: UIImageView!
    @IBOutlet var lblObjectNumber: [UILabel]!
    @IBOutlet weak var lblPeople: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblClassification: UILabel!
    @IBOutlet weak var lblWorkType: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblCulture: UILabel!
    @IBOutlet weak var lblMedium: UILabel!
    @IBOutlet weak var lblAccessionYear: UILabel!
    @IBOutlet weak var lblDivision: UILabel!
    @IBOutlet weak var btnURL: UIButton!
    
    //MARK: - App Life Cicle
	override func viewDidLoad() {
        super.viewDidLoad()
        self.setupViewComponents()
        self.title = "Detail"
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Functions
    fileprivate func setupViewComponents() {
        self.detailArt.forEach { (record) in
            if record.primaryURL != nil {
                self.imgArt.sd_setShowActivityIndicatorView(true)
                self.imgArt.sd_setIndicatorStyle(.gray)
                self.imgArt.sd_setImage(with: URL(string: record.primaryURL!), placeholderImage: nil, options: .cacheMemoryOnly, completed: nil)
            }else {
                self.imgArt.image = UIImage(named: "no-image")
            }
            
            for obj in self.lblObjectNumber.indices {
                self.lblObjectNumber[obj].text = "Object Number: \(record.objectNumber ?? "---")"
            }
            
            record.people?.forEach({ (people) in
                self.lblPeople.text = "People: \(people.displayName ?? "---")"
            })
            
            self.lblTitle.text = "Title: \(record.title ?? "---")"
            self.lblClassification.text = "Classification: \(record.classification ?? "---")"
            self.lblWorkType.text = "Work Type: \(record.technique ?? "---")"
            self.lblDate.text = "Date: \(record.dated ?? "---")"
            self.lblCulture.text = "Culture: \(record.culture ?? "---")"
            self.lblMedium.text = "Medium: \(record.medium ?? "---")"
            self.lblDivision.text = "Division: \(record.division ?? "---")"
            
            if record.accessionYear != nil {
                self.lblAccessionYear.text = "Accession Year:\(record.accessionYear!)"
            }else {
                self.lblAccessionYear.text = "---"
            }
            
            if record.url != nil {
                self.btnURL.isHidden = false
                self.btnURL.isEnabled = true
                self.btnURL.setTitle("Go to URL", for: .normal)
            }else {
                self.btnURL.isEnabled = false
                self.btnURL.isHidden = true
            }
        }
    }
    @IBAction func goToArtURL() {
        self.detailArt.forEach { (record) in
            if record.url != nil {
                UIApplication.shared.open(URL(string: record.url!)!, options: [:], completionHandler: nil)
            }
        }
    }
}

//MARK: - Extension to implement HarvardArtDetailViewControllerProtocol
extension HarvardArtDetailViewController: HarvardArtDetailViewControllerProtocol {
    
}
