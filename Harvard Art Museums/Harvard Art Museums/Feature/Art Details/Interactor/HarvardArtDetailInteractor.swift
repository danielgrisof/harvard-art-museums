//
//  HarvardArtDetailInteractor.swift
//  Harvard Art Museums
//
//  Created Daniel Griso Filho on 7/1/18.
//  Copyright © 2018 Daniel Griso Filho. All rights reserved.


import Foundation

class HarvardArtDetailInteractor {

    //MARK: Local Variable
    var presenter: HarvardArtDetailPresenterProtocol?
    
    //MARK: - Functions
    
}

//MARK: - Extension to implement HarvardArtDetailInteractorProtocol
extension HarvardArtDetailInteractor: HarvardArtDetailInteractorProtocol {
    
}
