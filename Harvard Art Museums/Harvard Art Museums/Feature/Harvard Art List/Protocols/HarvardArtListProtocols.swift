//
//  HarvardArtListProtocols.swift
//  Harvard Art Museums
//
//  Created Daniel Griso Filho on 6/30/18.
//  Copyright © 2018 Daniel Griso Filho. All rights reserved.

import Foundation

//MARK: - Router
protocol HarvardArtListRouterProtocol {
    func goToDetail(art: [Records])
}
//MARK: - Presenter
protocol HarvardArtListPresenterProtocol {
    func requestArtListOf(page: Int)
    func resultFromRequestOf(art: [Records])
    func resultFromRequestFail(title: String, message: String)
    func detailArtOf(art: [Records])
}

//MARK: - Interactor
protocol HarvardArtListInteractorProtocol{
    func callAPIOf(page: Int)
}

//MARK: - View
protocol HarvardArtListViewControllerProtocol{
    func harvardListOf(art: [Records])
    func showAlert(title: String, message: String)
}
