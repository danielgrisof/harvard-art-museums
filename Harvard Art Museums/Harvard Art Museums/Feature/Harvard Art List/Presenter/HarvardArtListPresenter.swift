//
//  HarvardArtListPresenter.swift
//  Harvard Art Museums
//
//  Created Daniel Griso Filho on 6/30/18.
//  Copyright © 2018 Daniel Griso Filho. All rights reserved.

import UIKit

class HarvardArtListPresenter {

    //MARK: Local Variable
    var view: HarvardArtListViewControllerProtocol?
    var interactor: HarvardArtListInteractorProtocol?
    var router: HarvardArtListRouterProtocol?

    //MARK: Initializer
    init(view: HarvardArtListViewControllerProtocol, interactor: HarvardArtListInteractorProtocol, router: HarvardArtListRouterProtocol) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    //MARK: - Functions
    fileprivate func requestListOf(page: Int) {
        self.interactor?.callAPIOf(page: page)
    }
    
    fileprivate func successResultOf(art: [Records]) {
        if art.count > 0 {
            self.view?.harvardListOf(art: art)
        }
    }
    
    fileprivate func failRequest(title: String, message: String) {
        self.view?.showAlert(title: title, message: message)
    }
    
    fileprivate func artDetailsOf(art: [Records]) {
        self.router?.goToDetail(art: art)
    }
}

//MARK: - Extension to implement HarvardArtListPresenterProtocol
extension HarvardArtListPresenter: HarvardArtListPresenterProtocol {
    func requestArtListOf(page: Int) {
        self.requestListOf(page: page)
    }
    
    func resultFromRequestOf(art: [Records]) {
        self.successResultOf(art: art)
    }
    
    func resultFromRequestFail(title: String, message: String) {
        self.failRequest(title: title, message: message)
    }
    
    func detailArtOf(art: [Records]) {
        self.artDetailsOf(art: art)
    }
}
