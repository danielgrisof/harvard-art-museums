//
//  HarvardArtListInteractor.swift
//  Harvard Art Museums
//
//  Created Daniel Griso Filho on 6/30/18.
//  Copyright © 2018 Daniel Griso Filho. All rights reserved.


import Foundation
import Alamofire

class HarvardArtListInteractor {

    //MARK: Local Variable
    var presenter: HarvardArtListPresenterProtocol?
    let generic = GenericRequest()
    
    //MARK: - Functions
    fileprivate func requestAPIOf(page: Int) {
        
        let parameters = ["apikey": "30d7eff0-7cab-11e8-aede-f9400dea7b1a", "page": page, "size": 100] as [String: AnyObject]
        
        generic.apiRequest(method: HTTPMethod.get, forService: RESOURCE_OBJECT, parameters: parameters, completion: { (arts: HarvardArtListEntity) in
            if let art = arts.records {
                self.presenter?.resultFromRequestOf(art: art)
            }else {
                self.presenter?.resultFromRequestFail(title: "Error", message: "Something Went Wrong with the API call")
            }
        }) { (error) in
            self.presenter?.resultFromRequestFail(title: "Error", message: error.description)
        }
        
    }
    
}

//MARK: - Extension to implement HarvardArtListInteractorProtocol
extension HarvardArtListInteractor: HarvardArtListInteractorProtocol {
    
    func callAPIOf(page: Int) {
        self.requestAPIOf(page: page)
    }
}
