//
//  HarvardArtListViewController.swift
//  Harvard Art Museums
//
//  Created Daniel Griso Filho on 6/30/18.
//  Copyright © 2018 Daniel Griso Filho. All rights reserved.

import UIKit

class HarvardArtListViewController: UIViewController {

    //MARK: Local Variable
	var presenter: HarvardArtListPresenterProtocol?
    let cellIdentifier = "artCell"
    var arts = [Records]()
    var page = 1
    
    //MARK: Local Outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    //MARK: - App Life Cicle
	override func viewDidLoad() {
        super.viewDidLoad()
        self.registerCell()
        self.activityIndicator.startAnimating()
        self.presenter?.requestArtListOf(page: page)
        self.tableView.tableFooterView = UIView()
        self.setupNavigationBar()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Functions
    fileprivate func registerCell() {
        tableView.register(UINib(nibName: "HarvardArtListCustomCell", bundle: nil), forCellReuseIdentifier: cellIdentifier)
    }
    
    fileprivate func setupNavigationBar() {
        self.title = "Test"
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.barTintColor = UIColor.black
        let backButton = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        backButton.tintColor = UIColor.white
        self.navigationItem.backBarButtonItem = backButton
    }
}

//MARK: - Extension to implement HarvardArtListViewControllerProtocol
extension HarvardArtListViewController: HarvardArtListViewControllerProtocol {
    func harvardListOf(art: [Records]) {
        if arts.count == 0 {
            self.arts = art
        }else {
            self.arts.append(contentsOf: art)
        }
        self.activityIndicator.stopAnimating()
        self.tableView.reloadData()
    }
    func showAlert(title: String, message: String) {
        self.alert(title: title, message: message)
    }
}

//MARK: - Extension to implement UITableViewDelegate, UITableViewDataSource
extension HarvardArtListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? HarvardArtListCustomCell {
            cell.configureCellWith(art: arts[indexPath.row])
            return cell
        }else {
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if (indexPath.row + 3) == self.arts.count {
            page += 1
            self.presenter?.requestArtListOf(page: page)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        self.presenter?.detailArtOf(art: [self.arts[indexPath.row]])
    }
}
