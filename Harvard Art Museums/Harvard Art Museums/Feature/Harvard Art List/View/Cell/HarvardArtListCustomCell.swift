//
//  HarvardArtListCustomCell.swift
//  Harvard Art Museums
//
//  Created by Daniel Griso Filho on 6/30/18.
//  Copyright © 2018 Daniel Griso Filho. All rights reserved.
//

import UIKit
import SDWebImage

class HarvardArtListCustomCell: UITableViewCell {

    //MARK: Local Outlets
    @IBOutlet weak var lblPeopleName: UILabel!
    @IBOutlet weak var lblYear: UILabel!
    @IBOutlet weak var lblTechnique: UILabel!
    @IBOutlet weak var lblCuture: UILabel!
    @IBOutlet weak var lblClassification: UILabel!
    @IBOutlet weak var imgPrimaryImageURL: UIImageView!
    
    //MARK: - Functions
    func configureCellWith(art: Records) {
        self.lblTechnique.text = art.technique ?? "---"
        self.lblCuture.text = art.culture ?? "---"
        self.lblClassification.text = art.classification ?? "---"
        
        art.people?.forEach({ (people) in
            self.lblPeopleName.text = people.name ?? "---"
        })
        
        if art.accessionYear != nil {
            self.lblYear.text = "\(art.accessionYear!)"
        }else {
            self.lblYear.text = "---"
        }
        
        if art.primaryURL != nil {
            self.imgPrimaryImageURL.sd_setShowActivityIndicatorView(true)
            self.imgPrimaryImageURL.sd_setIndicatorStyle(.gray)
            self.imgPrimaryImageURL.sd_setImage(with: URL(string: art.primaryURL!), placeholderImage: nil, options: .highPriority, completed: nil)
        
        }else {
            self.imgPrimaryImageURL.image = UIImage(named: "no-image")
        }
    }
    
}
