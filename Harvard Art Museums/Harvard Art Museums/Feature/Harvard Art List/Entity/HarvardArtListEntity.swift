//
//  HarvardArtListEntity.swift
//  Harvard Art Museums
//
//  Created Daniel Griso Filho on 6/30/18.
//  Copyright © 2018 Daniel Griso Filho. All rights reserved.


import Foundation
import ObjectMapper

class HarvardArtListEntity: Mappable {

    //MARK: Local Variable
    var records: [Records]?
    
    //MARK: - Initializer
    required init?(map: Map) {
        
    }
    
    //MARK: - Functions
    func mapping(map: Map) {
        self.records <- map["records"]
    }
}

class Records: Mappable {
    
    //MARK: Local Variable
    var accessionYear: Int?
    var technique: String?
    var mediaCount: Int?
    var edition: String?
    var totalPagesViews: Int?
    var groupCount: Int?
    var people: [People]?
    var objectNumber: String?
    var lastUpdate: String?
    var ranking: Int?
    var imageCount: Int?
    var description: String?
    var primaryURL: String?
    var dated: String?
    var period: String?
    var accessionMethod: String?
    var url: String?
    var culture: String?
    var department: String?
    var title: String?
    var division: String?
    var style: String?
    var century: String?
    var medium: String?
    var classification: String?
    
    //MARK: - Initializer
    required init?(map: Map) {
        
    }
    
    //MARK: - Functions
    func mapping(map: Map) {
        self.accessionYear <- map["accessionyear"]
        self.technique <- map["technique"]
        self.mediaCount <- map["mediacount"]
        self.edition <- map["edition"]
        self.totalPagesViews <- map["totalpageviews"]
        self.groupCount <- map["groupcount"]
        self.people <- map["people"]
        self.objectNumber <- map["objectnumber"]
        self.lastUpdate <- map["lastupdate"]
        self.ranking <- map["ranking"]
        self.imageCount <- map["imagecount"]
        self.description <- map["description"]
        self.primaryURL <- map["primaryimageurl"]
        self.dated <- map["dated"]
        self.period <- map["period"]
        self.accessionMethod <- map["accessionmethod"]
        self.url <- map["url"]
        self.culture <- map["culture"]
        self.department <- map["department"]
        self.title <- map["title"]
        self.division <- map["division"]
        self.style <- map["style"]
        self.century <- map["century"]
        self.medium <- map["medium"]
        self.classification <- map["classification"]
    }
}

class People: Mappable {
    
    //MARK: Local Variable
    var alphaSort: String?
    var birthPlace: String?
    var name: String?
    var gender: String?
    var role: String?
    var culture: String?
    var displayName: String?
    
    //MARK: - Initializer
    required init?(map: Map) {
        
    }
    
    //MARK: - Functions
    func mapping(map: Map) {
        self.alphaSort <- map["alphasort"]
        self.birthPlace <- map["birthplace"]
        self.name <- map["name"]
        self.gender <- map["gender"]
        self.role <- map["role"]
        self.culture <- map["culture"]
        self.displayName <- map["displayname"]
    }
}
