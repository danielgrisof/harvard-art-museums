//
//  HarvardArtListRouter.swift
//  Harvard Art Museums
//
//  Created Daniel Griso Filho on 6/30/18.
//  Copyright © 2018 Daniel Griso Filho. All rights reserved.


import UIKit

class HarvardArtListRouter: BaseRouter {
    
    //MARK: - Functions
    
    static func createModule() -> HarvardArtListViewController {

        let viewController: HarvardArtListViewController = HarvardArtListViewController()
        let router: HarvardArtListRouter = HarvardArtListRouter(viewController: viewController)
        let interactor: HarvardArtListInteractor = HarvardArtListInteractor()
        let presenter: HarvardArtListPresenter = HarvardArtListPresenter(view: viewController, interactor: interactor, router: router)
        
        viewController.presenter = presenter
        interactor.presenter = presenter
        router.viewController = viewController
        
        return viewController
    }
    
    fileprivate func showDetailOf(art: [Records]) {
        let vc = HarvardArtDetailRouter.createModule()
        vc.detailArt = art
        self.pushToView(viewController: vc, animated: true)
    }
}

//MARK: - Extension to implement HarvardArtListRouterProtocol
extension HarvardArtListRouter: HarvardArtListRouterProtocol {
    func goToDetail(art: [Records]) {
        self.showDetailOf(art: art)
    }
}
