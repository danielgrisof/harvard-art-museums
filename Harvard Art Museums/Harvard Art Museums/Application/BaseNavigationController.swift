//
//  BaseNavigationController.swift
//  Marvel
//
//  Created by Daniel Griso Filho on 5/19/18.
//  Copyright © 2018 Daniel Griso Filho. All rights reserved.
//

import UIKit

class BaseNavigationController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
