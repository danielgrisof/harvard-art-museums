//
//  ServiceDefinitions.swift
//  Harvard Art Museums
//
//  Created by Daniel Griso Filho on 6/30/18.
//  Copyright © 2018 Daniel Griso Filho. All rights reserved.
//

import Foundation

let BASE_URL = "https://api.harvardartmuseums.org"
let RESOURCE_OBJECT = "/object"
