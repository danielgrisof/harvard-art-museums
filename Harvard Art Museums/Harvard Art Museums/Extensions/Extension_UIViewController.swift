//
//  Extension_UIViewController.swift
//  Global Restaurants
//
//  Created by Daniel Griso Filho on 6/24/18.
//  Copyright © 2018 Daniel Griso Filho. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func alert(title: String, message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let firstButton = UIAlertAction(title: "Ok", style: .default)
        alertController.addAction(firstButton)
        self.present(alertController, animated: true, completion: nil)
    }
}
