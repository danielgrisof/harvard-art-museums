//
//  GenericRequest.swift
//  Harvard Art Museums
//
//  Created by Daniel Griso Filho on 6/30/18.
//  Copyright © 2018 Daniel Griso Filho. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper

class GenericRequest {
    
    func apiRequest<T: Mappable>(method: HTTPMethod, forService: String, parameters: [String: AnyObject], completion: @escaping (T) -> (), error: @escaping (NSError) -> ()) {
        
        let url = URL(string: "\(BASE_URL)\(forService)")!
        
        Alamofire.request(url, method: method, parameters: parameters).responseJSON { (response: DataResponse<Any>) in
            if response.error != nil {
                let requestError = ApiResponse.StatusCode.DomainError.Unknown
                error(NSError(domain: requestError.0, code: requestError.1, userInfo: ["message": requestError.2]))
                return
            }
            if response.response?.statusCode == ApiResponse.StatusCode.Successful.OK {
                let result = response.result.value
                let jsonObject = Mapper<T>().map(JSONObject: result)
                completion(jsonObject!)
            }else{
                let requestError = ApiResponse.StatusCode.DomainError.Unknown
                error(NSError(domain: requestError.0, code: requestError.1, userInfo: ["message": requestError.2]))
            }
        }
    }
    
}
